package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.Implementation.JDisplay";
    public static String controlPanelImplClassName = "pk.labs.LabB.Implementation.JControlPanel";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Myjnia";
    public static String mainComponentImplClassName = "pk.labs.LabB.Implementation.JMyjnia";
    public static String mainComponentBeanName = "nowamyjnia";
    // endregion

    // region P2
    public static String mainComponentMethodName = "...";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
